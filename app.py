from glob import glob
import os
import numpy as np
import pandas as pd
import random
from skimage.io import imread
import matplotlib.pyplot as plt
from tensorflow.keras.models import load_model
from tensorflow.keras.applications import ResNet50
from tensorflow.keras.applications.resnet50 import preprocess_input, decode_predictions
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten, GlobalAveragePooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img
from flask import Flask, redirect, url_for, request, render_template
from werkzeug.utils import secure_filename

app = Flask(__name__)

MODEL_PATH = r'xray_model.h5'
model = load_model(MODEL_PATH)

@app.route('/', methods=['GET'])
def index():
    # Main page
    return render_template('index.html')


@app.route('/predict', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        # Get the file from post request
        f = request.files['file']

        # Save the file to ./uploads
        basepath = os.path.dirname(__file__)
        file_path = os.path.join(
            basepath, 'uploads', secure_filename(f.filename))
        
        f.save(file_path)


        normal_or_pneumonia = ['NORMAL', 'PNEUMONIA']
        #pneumonia_images = 'H:/samhitha/person22_virus_55.jpeg'
        img_choice = (file_path)
        img = load_img(img_choice, target_size=(150, 150))
        img = img_to_array(img)
        plt.imshow(img / 255.)
        x = preprocess_input(np.expand_dims(img.copy(), axis=0))
        pred_class = model.predict_classes(x)
        pred = model.predict(x)
        text=""
        #print("Actual class:", folder_choice)
        if pred_class[0] == 0:
            text="Predicted class: Normal, Likelihood:"+str(pred[0][0].round(4)) 
            if pred[0][0].round(4) < 0.8:
                text+="with the WARNING of low confidence"            
        else:
            text="Predicted class: Pneumonia, Likelihood:"+str(pred[0][1].round(4))
            if pred[0][1].round(4) < 0.8:
                text+="WARNING, low confidence"      

        return text

if __name__ == '__main__':
    app.run(debug=False,threaded = False)
