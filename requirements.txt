
Flask==1.1.2
numpy==1.18.5
scipy==1.4.1
scikit-image==0.17.2
matplotlib==3.2.2
scikit-learn==0.20.0
Keras==2.1.5
tensorflow==2.2.0rc2
gevent==20.6.2
Werkzeug==1.0.1